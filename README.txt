# Spring-ORM + JPA

- 2013/Feb/02/Sat

  * Spring-ORM + Hibernate/JPA.

  * Spring-Test.

  * JPA DateTime.
  * Named Query.
  * LOB Support.

  * Spring-Tx : @Transactional Test.
        -- 반드시 DAO등에서 잘 고려하여 트랜잭션 nesting하도록.


-- TODOs

  * LOB without byte[]?

  * Simple Mappings + Relations.

  * Rails-like environments.

  * <context:component-scan base-package="jhyun"/> --> spel props? 이거 왜 안먹는지 모르겠음.

###EOF