package jhyun.spring_orm_jpa_hello.entities;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 게시물 엔티티.
 *
 * @author jhyun
 */
@NamedQueries({
    @NamedQuery(name = "countAllPosts", query = "select count(p) from Post p"),
    @NamedQuery(name = "deleteAllPosts", query = "delete from Post p")
})
@Entity
@Table(name = "POST")
public class Post {

    /**
     * 자동 id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    public int id;
    /**
     * 제목.
     */
    @Column(name = "TITLE", nullable = false)
    public String title;
    /**
     * CLOB/TEXT등의 텍스트를 매핑.
     *
     * 원래의 테이블스키마가 있다면 적당히 매핑?
     */
    @Column(name = "MESSAGE", nullable = false)
    @Lob
    public String message;
    /**
     * SQL TIMESTAMP 타입을 java.util.Date으로 매핑.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_CREATED_OR_UPDATED")
    public Date lastCreatedOrUpdated;
}
