package jhyun.spring_orm_jpa_hello.spring;

import java.util.Properties;
import javax.persistence.spi.PersistenceProvider;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@EnableTransactionManagement
@Configuration
public class JpaConfig {

    @Autowired
    private DataSource dataSource;
    @Value("#{jpaProperties.packagesToScan}")
    private String packagesToScan;
    @Value("#{jpaProperties.persistenceUnitName}")
    private String persistenceUnitName;
    @Value("#{jpaProperties.persistenceProviderClassName}")
    private String persistenceProviderClassName;
    @Autowired
    private Properties jpaProperties;
    private Logger log = LoggerFactory.getLogger(JpaConfig.class);

    @DependsOn(value = {"dataSource", "jpaProperties"})
    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean emf =
                new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(dataSource);
        emf.setPackagesToScan(packagesToScan);
        emf.setJpaProperties(jpaProperties);
        try {
            emf.setPersistenceProviderClass((Class<? extends PersistenceProvider>) Class.forName(persistenceProviderClassName));
        } catch (ClassNotFoundException ex) {
            log.error("persistenceProviderClassName FAIL!", ex);
        }
        emf.setPersistenceUnitName(persistenceUnitName);
        return emf;
    }

    @Bean
    @DependsOn("entityManagerFactory")
    public JpaTransactionManager transactionManager() {
        return new JpaTransactionManager(entityManagerFactory().getObject());
    }
}
