package jhyun.spring_orm_jpa_hello.dao;

import java.util.Date;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import jhyun.spring_orm_jpa_hello.entities.Post;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jhyun
 */
@Transactional(rollbackFor = {Throwable.class})
@Component
public class PostDao {

    @PersistenceContext
    private EntityManager em;

    public void deleteAllPosts() {
        em.createNamedQuery("deleteAllPosts").executeUpdate();
    }

    @Transactional(readOnly = true)
    public long countAllPosts() {
        return (Long) em.createNamedQuery("countAllPosts").getSingleResult();
    }
    
    public Post save(Post p) {
        return em.merge(p);        
    }
    
    public Post save3() throws Exception {
        for (int n = 0; n < 3; n++) {
            Post p = new Post();
            String s = String.format("%s", n);
            p.title = s;
            p.message = s;
            p.lastCreatedOrUpdated = new Date();
            em.merge(p);
        }
        throw new Exception("OK");
    }
    
}
