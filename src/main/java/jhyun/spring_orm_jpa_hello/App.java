package jhyun.spring_orm_jpa_hello;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class App {

    public static void main(String[] args) {
        ApplicationContext appCtx =
                new ClassPathXmlApplicationContext("/jhyun/spring_orm_jpa_hello/spring/applicationContext.xml");
    }
}
