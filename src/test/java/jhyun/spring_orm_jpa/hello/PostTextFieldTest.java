package jhyun.spring_orm_jpa.hello;

import java.util.Date;
import java.util.Properties;
import jhyun.spring_orm_jpa_hello.dao.PostDao;
import jhyun.spring_orm_jpa_hello.entities.Post;
import jhyun.spring_slf4j_annotation.Loggable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author adaltan
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {TestCaseConsts.applicationContextPath})
public class PostTextFieldTest {

    @Loggable
    private Logger log;
    @Autowired
    private PostDao postDao;
    @Autowired
    private Properties contextProperties;

    @Test
    public void alwaysOk() {
        Assert.assertTrue(true);
    }

    @Test
    public void injectedNotNulls() {
        Assert.assertNotNull(log);
        Assert.assertNotNull(postDao);


    }

    @Transactional
    @Test
    public void postAndDeletePost() {

        postDao.deleteAllPosts();

        Post p = new Post();
        p.title = "title";
        p.message = "message";
        p.lastCreatedOrUpdated = new Date();
        postDao.save(p);
        
        final long countAllPosts = postDao.countAllPosts();
        Assert.assertEquals(1, countAllPosts);
    }

    @Test
    public void rollbackOk() {
        final long beforeCount = postDao.countAllPosts();
        try {
            postDao.save3();
        } catch (Exception exc) {
            log.debug("OK", exc);
        }
        final long afterCount = postDao.countAllPosts();
        Assert.assertEquals(beforeCount, afterCount);
    }

}
